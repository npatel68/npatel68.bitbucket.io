/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME405: Mechatronics", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Source Code Repository", "index.html#repo", null ],
    [ "HW0x02: Term Project System Modeling", "hw0x02.html", null ],
    [ "HW0x04: Simulation or Reality?", "hw0x04.html", [
      [ "Overview", "hw0x04.html#sec_hw0x04_overview", null ],
      [ "Case 1: Open Loop with Zero Initial Conditions", "hw0x04.html#sec_hw0x04_case1", null ],
      [ "Case 2: Open Loop with x_i = 5 cm", "hw0x04.html#sec_hw0x04_case2", null ],
      [ "Case 3: Closed Loop with x_i = 5 cm and T_x = -Kx", "hw0x04.html#sec_hw0x04_case3", null ],
      [ "Source Code", "hw0x04.html#hw0x04_source", null ]
    ] ],
    [ "HW0x05: Full State Feedback", "hw0x05.html", [
      [ "Overview", "hw0x05.html#sec_hw0x05_overview", null ],
      [ "Simulation Results and Performance", "hw0x05.html#sec_hw0x05_res", null ],
      [ "Source Code", "hw0x05.html#hw0x05_source", null ]
    ] ],
    [ "Lab0x01: FSM Practice and User Interface Development", "lab0x01.html", [
      [ "Summary", "lab0x01.html#sec_lab0x01_summary", null ],
      [ "Design Features", "lab0x01.html#sec_lab0x01_features", null ],
      [ "User Inputs", "lab0x01.html#sec_lab0x01_inputs", null ],
      [ "Vendotron State Transition Diagram", "lab0x01.html#sec_lab0x01_diagram", null ],
      [ "Documentation", "lab0x01.html#lab0x01_documentation", null ],
      [ "Source Code", "lab0x01.html#lab0x01_source", null ]
    ] ],
    [ "Lab0x02: Interrupt Service Routines and Timing", "lab0x02.html", [
      [ "Summary", "lab0x02.html#sec_lab0x02_summary", null ],
      [ "Design Features - Part A", "lab0x02.html#sec_lab0x02_features_a", null ],
      [ "Design Features - Part B", "lab0x02.html#sec_lab0x02_features_b", null ],
      [ "Overview", "lab0x02.html#sec_lab0x02_overview", null ],
      [ "Documentation", "lab0x02.html#lab0x02_documentation", null ],
      [ "Source Code", "lab0x02.html#lab0x02_source", null ]
    ] ],
    [ "Lab0x03: Pushing the Right Buttons", "lab0x03.html", [
      [ "Summary", "lab0x03.html#sec_lab0x03_summary", null ],
      [ "Design Features - The Button Circuit", "lab0x03.html#sec_lab0x03_features_a", null ],
      [ "Design Features - Analog-to-Digital Conversion", "lab0x03.html#sec_lab0x03_features_b", null ],
      [ "Design Features - Serial Communication", "lab0x03.html#sec_lab0x03_features_c", null ],
      [ "Overview", "lab0x03.html#sec_lab0x03_overview", null ],
      [ "Documentation", "lab0x03.html#lab0x03_documentation", null ],
      [ "Source Code", "lab0x03.html#lab0x03_source", null ]
    ] ],
    [ "Lab0x04: Hot or Not?", "lab0x04.html", [
      [ "Summary", "lab0x04.html#sec_lab0x04_summary", null ],
      [ "Design Features", "lab0x04.html#sec_lab0x04_features", null ],
      [ "Overview", "lab0x04.html#sec_lab0x04_overview", null ],
      [ "Documentation", "lab0x04.html#lab0x04_documentation", null ],
      [ "Source Code", "lab0x04.html#lab0x04_source", null ]
    ] ],
    [ "Lab0xFF: Term Project", "lab0xff.html", [
      [ "Summary", "lab0xff.html#sec_lab0xff_summary", null ],
      [ "Design Features", "lab0xff.html#sec_lab0xff_features", null ],
      [ "Touch Panel", "lab0xff.html#sec_lab0xff_touch_panel", [
        [ "Calibration of Touch Panel", "lab0xff.html#subsec_tp_calib", null ],
        [ "Benchmarks for Timing", "lab0xff.html#subsec_tp_bench", [
          [ "Scan_Z Test:", "lab0xff.html#sss_scanz", null ],
          [ "Scan_Y Test:", "lab0xff.html#sss_scany", null ],
          [ "Scan_X Test:", "lab0xff.html#sss_scanx", null ],
          [ "Scan_ALL Test:", "lab0xff.html#sss_scan_all", null ],
          [ "Summary of Benchmarks", "lab0xff.html#sss_summary", null ]
        ] ],
        [ "Usage of Touch Panel Driver", "lab0xff.html#subsec_tp_usage", null ]
      ] ],
      [ "Motors", "lab0xff.html#sec_lab0xff_motors", [
        [ "Video Demonstration", "lab0xff.html#subsec_mot_video", null ],
        [ "Dutycycle Benchmark", "lab0xff.html#subsec_mot_benchmark", null ]
      ] ],
      [ "Encoders", "lab0xff.html#sec_lab0xff_encoders", null ],
      [ "Controller", "lab0xff.html#sec_lab0xff_controller", null ],
      [ "Scheduler", "lab0xff.html#sec_lab0xff_scheduler", null ],
      [ "State Transition Diagram", "lab0xff.html#sec_lab0xff_std", null ],
      [ "Video Demonstration", "lab0xff.html#sec_lab0xff_vd", null ],
      [ "Platform Data", "lab0xff.html#sec_lab0xff_pd", null ],
      [ "Major Issues", "lab0xff.html#sec_lab0xff_mi", [
        [ "Touch Panel Issues", "lab0xff.html#subsec_tp_issues", null ],
        [ "Motor Issues", "lab0xff.html#subsec_mot_issues", null ],
        [ "Encoder Issues", "lab0xff.html#subsec_enc_issues", null ],
        [ "Scheduler Issues", "lab0xff.html#subsec_sched_issues", null ],
        [ "Board Issues", "lab0xff.html#subsec_board_issues", null ]
      ] ],
      [ "Documentation", "lab0xff.html#lab0xff_documentation", null ],
      [ "Source Code", "lab0xff.html#lab0xff_source", null ],
      [ "Repositories", "lab0xff.html#lab0xff_repos", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"UI__front_8py.html",
"lab2b_8py.html#a97ad1e4fbbac7a8d6d95ed0fb871d6b4"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';